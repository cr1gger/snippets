# Смена раскладки клавиатуры на Alt+Shift
Через программу dconf-editor выставить <br>
Для xkb-options  = `[]` - пустое значение, т.е отключить <br>
Для switch-input-source = `['<Alt>Shift_L', '<Shift>Alt_L']`
